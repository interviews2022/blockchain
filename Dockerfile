FROM node 
WORKDIR /blockchain 
COPY package.json . 
RUN yarn install 
COPY . . 
EXPOSE 8080 
CMD yarn start